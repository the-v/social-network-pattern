import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { addPost } from "./Redux/State";

export function rerenderAllTree(state) {
    ReactDOM.render(
        <React.StrictMode>
            <App appState={state} addPost={addPost} />
        </React.StrictMode>,
        document.getElementById('root')
    );
}