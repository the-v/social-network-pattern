import React from "react";
import styles from "./Messages.module.css";
import Message from "./Message/Message";

function Messages(props) {
    let messagesElement = props.messagesData.map(message => <Message id={message.id}
                                                                     userAvatar={props.userData.userAvatar}
                                                                     userName={props.userData.userName}
                                                                     message1={message.message1}/>) ;

    return(
        <div className={styles.messages}>
            {messagesElement}
        </div>
    );
}

export default Messages;