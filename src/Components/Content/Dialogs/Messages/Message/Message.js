import React from "react";
import styles from "./Message.module.css";
import {NavLink} from "react-router-dom";

function Message(props) {
    return(
            <NavLink to={"/dialogs/" + props.id}>
                <div className={styles.message}>
                    <div className={styles.userAvatar}><img src={props.userAvatar} alt=""/></div>

                    <div className={styles.wrapper}>
                        <span className={styles.userName}>{props.userName}</span>
                        <div className={styles.userMessage}>{props.message1}</div>
                    </div>
                </div>
            </NavLink>
    );
}

export default Message;