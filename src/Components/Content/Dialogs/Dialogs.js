import React from "react";
import styles from "./Dialogs.module.css";
import Messages from "./Messages/Messages";
import Windows from "./Windows/Windows";


function Dialogs(props) {
    return(
        <div className={styles.dialogs}>
            <Messages userData={props.userData} messagesData={props.messagesData}/>
            <Windows />
        </div>
    );
}

export default Dialogs;