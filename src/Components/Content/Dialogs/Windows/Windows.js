import React from "react";
import styles from "./Windows.module.css";
import Window from "./Window/Window";

function Windows(props) {
    return(
        <div className={styles.windows}>
            <Window dialogOfWindow="dialog's window of" id="1" />
            <Window dialogOfWindow="dialog's window of" id="2" />
            <Window dialogOfWindow="dialog's window of" id="3" />
            <Window dialogOfWindow="dialog's window of" id="4" />
            <Window dialogOfWindow="dialog's window of" id="5" />
        </div>
    );
}

export default Windows;