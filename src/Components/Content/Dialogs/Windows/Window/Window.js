import React from "react";
import {HashRouter, Route} from "react-router-dom";

function Window(props) {
    return(
        <HashRouter>
            <Route path={"/dialogs/" + props.id}
                   component={() => <div className="window">{props.dialogOfWindow + " " + props.id}</div>}
            />
        </HashRouter>
    );
}

export default Window;