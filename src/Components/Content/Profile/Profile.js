import React from "react";
import styles from "./Profile.module.css";
import CreatePost from "./Posts/CreatePost/CreatePost";
import MyPosts from "./Posts/MyPosts/MyPosts";
import ProfileInfo from "./ProfileInfo/ProfileInfo";

function Profile(props) {
    let posts = props.postData.map(post => <MyPosts userAvatar={props.userData.userAvatar}
                                                    userName={props.userData.userName}
                                                    message={post.message}/>)

    return(
        <div className="profile">
            <div className={styles.big_profile_photo}>
                <img src="https://image.jimcdn.com/app/cms/image/transf/none/path/sa6549607c78f5c11/image/i0e349bc866a994f4/version/1493651032/header-beach-2017.jpg" alt=""/>
            </div>

            <ProfileInfo userAvatar={props.userData.userAvatar}
                         userName={props.userData.userName}
                         property={props.property}
                         values={props.values} />

            <CreatePost addPost={props.addPost} />

            {posts}
        </div>
    );
}

export default Profile;