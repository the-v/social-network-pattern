import React from "react";
import styles from "./MyPosts.module.css";

function MyPosts(props) {
    return(
        <div className={styles.my_posts}>
            <div className={styles.poster}>
                <div className={styles.avatar}><img src={props.userAvatar} alt=""/></div>

                <div className={styles.name_content}>
                    <div className={styles.full_name}>{props.userName}</div>
                    <div className={styles.content}>{props.message}</div>
                </div>
            </div>
        </div>
    );
}

export default MyPosts;