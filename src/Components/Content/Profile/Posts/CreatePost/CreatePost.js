import React from "react";
import styles from "./CreatePost.module.css";

function CreatePost(props) {
    let newPostElement = React.createRef();

    function addPost() {
        let text = newPostElement.current.value;
        props.addPost(text);

        if (newPostElement) {
            newPostElement.current.value = '';
        }
    }
    
    function onChangePost() {

    }

    return(
        <div className={styles.create_post}>
            <span className={styles.title}>My Posts</span><br />

            <textarea className={styles.text_area}
                      ref={newPostElement}
                      onChange={onChangePost}
                      value={props.addPost.message} />

            <button className={styles.send} onClick={ addPost }>Send...</button>
        </div>
    );
}

export default CreatePost;