import React from "react";
import styles from "./ProfileInfo.module.css";
import PropertysValues from "./PropertysValues/PropertysValues";
import Portfolio from "./Portfolio/Portfolio";

function ProfileInfo(props) {
    return(
        <div className={styles.description_profile}>
            <div className={styles.avatar}><img src={props.userAvatar} alt=""/></div>

            <div className={styles.description}>
                <span className={styles.full_name}>{props.userName}</span>

                <PropertysValues property={props.property} values={props.values} />
                <Portfolio />
            </div>
        </div>
    );
}

export default ProfileInfo;