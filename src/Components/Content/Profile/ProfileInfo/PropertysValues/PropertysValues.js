import React from "react";
import styles from "./PropertysValues.module.css";

function PropertysValues(props) {
    return(
        <div className={styles.propertys_values}>
            <div className={styles.propertys}>
                <span className={styles.property}>{props.property.age}</span><br />
                <span className={styles.property}>{props.property.city}</span><br />
                <span className={styles.property}>{props.property.education}</span><br />
                <span className={styles.property}>{props.property.portfolio}</span>
            </div>

            <div className={styles.values}>
                <span className={styles.value}>{props.values.age}</span><br />
                <span className={styles.value}>{props.values.city}</span><br />
                <span className={styles.value}>{props.values.education}</span>
            </div>
        </div>
    );
}

export default PropertysValues;