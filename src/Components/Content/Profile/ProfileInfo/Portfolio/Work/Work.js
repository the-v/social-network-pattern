import React from "react";
import styles from "./Work.module.css";

function Work(props) {
    return(
        <div className={styles.work}>{props.workNumber}</div>
    );
}

export default Work;