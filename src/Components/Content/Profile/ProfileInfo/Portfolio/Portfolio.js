import React from "react";
import styles from "./Portfolio.module.css";
import Work from "./Work/Work";

function Portfolio() {
    let works = [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}];
    let work = works.map(work => <Work workNumber={work.id} />)

    return(
        <div className={styles.portfolio}>{work}</div>
    );
}

export default Portfolio;