import React from "react";
import Profile from "./Profile/Profile";
import Dialogs from "./Dialogs/Dialogs";

import {Route} from "react-router-dom";

function Content(props) {
    return(
            <div className="content">
                <Route path="/profile" render={() => <Profile userData={props.state.userData}
                                                              postData={props.state.profileData.postData}
                                                              property={props.state.profileData.property}
                                                              values={props.state.profileData.values}
                                                              addPost={props.addPost} />} />

                <Route path="/dialogs" render={() => <Dialogs userData={props.state.userData}
                                                              messagesData={props.state.messagesData} />} />
            </div>
    );
}

export default Content;