import React from "react";
import styles from "./SideBar.module.css";
import { NavLink } from "react-router-dom";

function SideBar() {
    return(
        <div className="sidebar">
                <NavLink to="/profile" className="active" aria-current="page">
                    <div className={styles.link}>Profile</div>
                </NavLink>

                <NavLink to="/dialogs">
                    <div className={styles.link}>Messages</div>
                </NavLink>
        </div>
    );
}

export default SideBar;