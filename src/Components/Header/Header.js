import React from "react";
import styles from "./Header.module.css";

function Header() {
    return(
        <header className="header">
            <span className={styles.logo_place}>[logotype place]</span>
        </header>
    );
}

export default Header;