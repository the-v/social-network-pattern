import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./Components/Header/Header";
import SideBar from "./Components/SideBar/SideBar";
import Content from "./Components/Content/Content";
import {HashRouter} from "react-router-dom";

function App(props) {
  return (
      <HashRouter>
        <div className="app-wrapper">
          <Header />
          <SideBar />

          <Content state={props.appState} addPost={props.addPost}/>
        </div>
      </HashRouter>
  );
}

export default App;
