import { rerenderAllTree } from "../render";
import userAvatar from "../img/userAvatar.png";

let state = {
    userData: {userAvatar: userAvatar, userName: "[user name]"},

    profileData: {
        property: {age: "Age:", city: "City:", education: "Education:", portfolio: "Portfolio:"},
        values: {age: 19, city: "Kyiv", education: "DAN.IT Education"},
        postData:[],
    },

    messagesData: [
        {id: 1, message1: "Hi, how are you?"},
        {id: 2, message1: "Гулять ідем?"},
        {id: 3, message1: "этоо господь господь"}
    ]
};

let count = 0 - 1;

export function addPost(postMessage) {
    count += 1;

    let newPost = {
        id: count,
        message: postMessage
    };

    state.profileData.postData.unshift(newPost);
    rerenderAllTree(state);

    if (newPost.message == '') {
        delete state.profileData.postData[0];
    }

    console.log(state.profileData.postData[0]);
}

export default state;

